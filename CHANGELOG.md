# Release Notes for 1.x

## [1.0.0 (2020-11-18)](https://bitbucket.org/teamhelium/com.helium.php.illuminate2/branches/compare/1.0.0%0D0.0.0#diff)
### Added
- Added ValidatesAttributes trait for Eloquent Models

## [1.0.1 (2020-11-30)](https://bitbucket.org/teamhelium/com.helium.php.illuminate2/branches/compare/1.0.1%0D1.0.0#diff)
### Added
- Added GuardsAttributesOnCreate trait for Eloquent Models

## [1.0.2 (2020-02-26)](https://bitbucket.org/teamhelium/com.helium.php.illuminate2/branches/compare/1.0.2%0D1.0.1#diff)
### Changed
- Renamed project to `com.helium.php.illuminate2`
- Renamed `CHANGELOG-1.x.md` to `CHANGELOG.md`
