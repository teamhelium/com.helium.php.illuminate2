<?php

namespace Helium\Illuminate2\Database\Eloquent\Concerns;

trait GuardsAttributesOnCreate
{
    /**
     * Get the fillable attributes for the model.
     *
     * @return array
     */
    public function getFillable()
    {
        $fillable = parent::getFillable();

        if (!$this->exists && $fillableOnCreate = $this->getFillableOnCreate()) {
            $fillable = array_merge($fillable, $fillableOnCreate);
        }

        return $fillable;
    }

    /**
     * Get fillable attributes for the model when it does not exist.
     *
     * @return array
     */
    public function getFillableOnCreate()
    {
        return $this->fillableOnCreate;
    }

    /**
     * Get the guarded attributes for the model.
     *
     * @return array
     */
    public function getGuarded()
    {
        $guarded = parent::getGuarded();

        if (!$this->exists && $guardedOnCreate = $this->getGuardedOnCreate()) {
            $guarded = array_merge($guarded, $guardedOnCreate);
        }

        return $guarded;
    }

    /**
     * Get the guarded attributes for the model when it does not exist.
     *
     * @return array
     */
    public function getGuardedOnCreate()
    {
        return $this->guardedOnCreate;
    }
}
