<?php

namespace Helium\Illuminate2\Database\Eloquent\Concerns;

use Illuminate\Contracts\Validation\Validator as ValidatorContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator as ValidatorFacade;

/**
 * @mixin Model
 */
trait ValidatesAttributes
{
    /**
     * Boot the trait.
     *
     * @return void
     */
    public static function bootValidatesAttributes()
    {
        static::saving(function ($model) {
            if ($model->getValidatesOnSave()) {
                $model->validate();
            }
        });
    }

    /**
     * Get Validator instance.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function getValidator(): ValidatorContract
    {
        return ValidatorFacade::make(
            $this->getAttributes(),
            $this->getValidationRules(),
            $this->getValidationMessages(),
            $this->getValidationCustomAttributes()
        );
    }

    /**
     * Get validates on save.
     *
     * @return bool
     */
    public function getValidatesOnSave(): bool
    {
        return $this->validatesOnSave ?? true;
    }

    /**
     * Get validation rules.
     *
     * @return array
     */
    public function getValidationRules(): array
    {
        return $this->validationRules ?? [];
    }

    /**
     * Get validation messages.
     *
     * @return array
     */
    public function getValidationMessages(): array
    {
        return $this->validationMessages ?? [];
    }

    /**
     * Get validation custom attributes.
     *
     * @return array
     */
    public function getValidationCustomAttributes(): array
    {
        return $this->validationCustomAttributes ?? [];
    }

    /**
     * Validate model attributes.
     *
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate(): array
    {
        return $this->getValidator()->validate();
    }
}
