<?php

namespace Tests\TestCases\Database\Eloquent\Concerns;

use Tests\Assets\Models\Database\Eloquent\Concerns\GuardsAttributesOnCreateModel;
use Tests\TestCases\TestCase;

class GuardsAttributesOnCreateTest extends TestCase
{
    public function testGetFillableOnCreate()
    {
        $model = new GuardsAttributesOnCreateModel();
        $this->assertEquals($model->fillableOnCreate, $model->getFillableOnCreate());
    }

    public function testGetGuardedOnCreate()
    {
        $model = new GuardsAttributesOnCreateModel();
        $this->assertEquals($model->guardedOnCreate, $model->getGuardedOnCreate());
    }

    public function testGetFillable()
    {
        $model = new GuardsAttributesOnCreateModel();
        $this->assertEquals(array_merge($model->fillable, $model->fillableOnCreate), $model->getFillable());

        $model->exists = true;
        $this->assertEquals($model->fillable, $model->getFillable());
    }

    public function testGetGuarded()
    {
        $model = new GuardsAttributesOnCreateModel();
        $this->assertEquals(array_merge($model->guarded, $model->guardedOnCreate), $model->getGuarded());

        $model->exists = true;
        $this->assertEquals($model->guarded, $model->getGuarded());
    }

    public function testFillOnCreate()
    {
        $model = new GuardsAttributesOnCreateModel();

        $model->fill([
            'string' => 'string',
            'integer' => 1,
            'boolean' => true,
            'date' => now()
        ]);

        $this->assertNotNull($model->string);
        $this->assertNotNull($model->integer);
        $this->assertNull($model->boolean);
        $this->assertNull($model->date);
    }

    public function testFillOnUpdate()
    {
        $model = new GuardsAttributesOnCreateModel();
        $model->exists = true;

        $model->fill([
            'string' => 'string',
            'integer' => 1,
            'boolean' => true,
            'date' => now()
        ]);

        $this->assertNull($model->string);
        $this->assertNotNull($model->integer);
        $this->assertNull($model->boolean);
        $this->assertNull($model->date);
    }

    public function testGuardedOnCreate()
    {
        $model = new GuardsAttributesOnCreateModel();
        $model->fillableOnCreate = [];
        $model->fillable = [];

        $model->fill([
            'string' => 'string',
            'integer' => 1,
            'boolean' => true,
            'date' => now()
        ]);

        $this->assertNotNull($model->string);
        $this->assertNotNull($model->integer);
        $this->assertNull($model->boolean);
        $this->assertNull($model->date);
    }

    public function testGuardedOnUpdate()
    {
        $model = new GuardsAttributesOnCreateModel();
        $model->exists = true;
        $model->fillableOnCreate = [];
        $model->fillable = [];

        $model->fill([
            'string' => 'string',
            'integer' => 1,
            'boolean' => true,
            'date' => now()
        ]);

        $this->assertNotNull($model->string);
        $this->assertNotNull($model->integer);
        $this->assertNotNull($model->boolean);
        $this->assertNull($model->date);
    }
}
