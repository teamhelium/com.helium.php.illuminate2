<?php

namespace Tests\TestCases\Database\Eloquent\Concerns;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Tests\Assets\Models\Database\Eloquent\Concerns\ValidatesAttributesModel;
use Tests\TestCases\TestCase;

class ValidatesAttributesTest extends TestCase
{
    public function testGetValidatesOnSave()
    {
        /**
         * Test getValidatesOnSave returns $validatesOnSave array on model.
         */
        $model = new ValidatesAttributesModel();
        $this->assertEquals($model->validatesOnSave, $model->getValidatesOnSave());

        /**
         * Test getValidatesOnSave returns true by default.
         */
        unset($model->validationRules);
        $this->assertEquals(true, $model->getValidatesOnSave());
    }

    public function testGetValidationRules()
    {
        /**
         * Test getValidationRules returns $validationRules array on model.
         */
        $model = new ValidatesAttributesModel();
        $this->assertEquals($model->validationRules, $model->getValidationRules());

        /**
         * Test getValidationRules returns empty array by default.
         */
        unset($model->validationRules);
        $this->assertEquals([], $model->getValidationRules());
    }

    public function testGetValidationMessages()
    {
        /**
         * Test getValidationMessages returns $validationMessages array on model.
         */
        $model = new ValidatesAttributesModel();
        $this->assertEquals($model->validationMessages, $model->getValidationMessages());

        /**
         * Test getValidationMessages returns empty array by default.
         */
        unset($model->validationMessages);
        $this->assertEquals([], $model->getValidationMessages());
    }

    public function testGetValidationCustomAttributes()
    {
        /**
         * Test getValidationCustomAttributes returns $validationCustomAttributes array on model.
         */
        $model = new ValidatesAttributesModel();
        $this->assertEquals($model->validationCustomAttributes, $model->getValidationCustomAttributes());

        /**
         * Test getValidationCustomAttributes returns empty array by default.
         */
        unset($model->validationCustomAttributes);
        $this->assertEquals([], $model->getValidationCustomAttributes());
    }

    public function testGetValidator()
    {
        /**
         * Test getValidator returns default Validator.
         */
        $model = new ValidatesAttributesModel();

        /** @var \Illuminate\Validation\Validator $validator */
        $validator = $model->getValidator();

        $this->assertInstanceOf(get_class(Validator::make([], [])), $validator);

        $modelValidationRules = array_map(function ($value) {
            return explode('|', $value);
        }, $model->getValidationRules());
        $this->assertEquals($modelValidationRules, $validator->getRules());
        $this->assertEquals($model->getAttributes(), $validator->getData());
        $this->assertEquals($model->getValidationMessages(), $validator->customMessages);
        $this->assertEquals($model->getValidationCustomAttributes(), $validator->customAttributes);
    }

    public function testValidateCalledOnSaveThrowsException()
    {
        $this->expectException(ValidationException::class);

        $model = new ValidatesAttributesModel([
            'string' => 123
        ]);
        $model->save();
    }

    public function testValidateNotCalledOnSavePasses()
    {
        $model = new ValidatesAttributesModel([
            'string' => 123
        ]);
        $model->validatesOnSave = false;

        $model->save();

        $this->assertTrue(true);
    }

    public function testValidateManuallyThrowsException()
    {
        $this->expectException(ValidationException::class);

        $model = new ValidatesAttributesModel([
            'string' => 123
        ]);

        $model->validate();
    }

    public function testValidatesOnSavePasses()
    {
        $model = new ValidatesAttributesModel();

        $model->save();

        $this->assertTrue(true);
    }
}
