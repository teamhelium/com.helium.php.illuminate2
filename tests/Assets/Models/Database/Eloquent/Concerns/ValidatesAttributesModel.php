<?php

namespace Tests\Assets\Models\Database\Eloquent\Concerns;

use Helium\Illuminate2\Database\Eloquent\Concerns\ValidatesAttributes;
use Tests\Assets\Models\TestModel;

class ValidatesAttributesModel extends TestModel
{
    use ValidatesAttributes;

    protected $attributes = [
        'string' => 'string',
        'integer' => 123,
        'boolean' => true
    ];

    public bool $validatesOnSave = true;

    public array $validationRules = [
        'string' => 'required|string',
        'integer' => 'required|integer',
        'boolean' => 'nullable|boolean'
    ];

    public array $validationMessages = [
        'string.required' => ':attribute required message',
        'string.string' => ':attribute string message',
        'integer.required' => ':attribute required message',
        'integer.integer' => ':attribute integer message',
        'boolean.boolean' => ':attribute boolean message',
    ];

    public array $validationCustomAttributes = [
        'string' => 'string attribute',
        'integer' => 'integer attribute',
        'boolean' => 'boolean attribute'
    ];
}
