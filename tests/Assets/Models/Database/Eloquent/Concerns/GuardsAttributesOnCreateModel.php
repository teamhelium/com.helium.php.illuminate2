<?php

namespace Tests\Assets\Models\Database\Eloquent\Concerns;

use Helium\Illuminate2\Database\Eloquent\Concerns\GuardsAttributesOnCreate;
use Tests\Assets\Models\TestModel;

class GuardsAttributesOnCreateModel extends TestModel
{
    use GuardsAttributesOnCreate;

    public $fillableOnCreate = [
        'string',
    ];

    public $fillable = [
        'integer'
    ];

    public $guardedOnCreate = [
        'boolean'
    ];

    public $guarded = [
        'date'
    ];
}
