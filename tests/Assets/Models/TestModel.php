<?php

namespace Tests\Assets\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TestModel
 *
 * @property string string
 * @property int integer
 * @property bool boolean
 * @property Carbon date
 */
class TestModel extends Model
{
    protected $table = 'test_models';

    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];
}
